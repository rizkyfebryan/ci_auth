<?php
		/**
		* 
		*/
		class Awalan extends CI_Controller
		{
			public function index($page=0)
			{
			if (!$this->ion_auth->logged_in())
			{
				// redirect them to the login per_page
				redirect('welcome/index', 'refresh');
			}																																						
				//meload model
				$this->load->library('pagination');
				$this->load->library('ion_auth');
				$this->load->model('datadaftar');
				$this->model=$this->datadaftar;

				//meload model pagination

				$row=$this->datadaftar->baris();
				$offset= $page;
				$config['base_url']=site_url('awalan/index');
				$config['total_rows'] = $row;
				$config['per_page'] = 5;
				$this->pagination->initialize($config);
				//cetak data dari model datadaftar, parameter sama dengan yang di model
				$cetak['cet']=$this->datadaftar->tampildata($config['per_page'],$offset);
				$this->form_validation->set_rules('nama', 'Nama', 'required');
				$this->form_validation->set_rules('email', 'Email','required|valid_email');
				$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		

				if ($this->form_validation->run() == FALSE)//jika salah mengisi data dari from daftar maka akan menampilkan validationnya
			        {

	
			          	$this->load->view('index',$cetak);      		
			        }

			        else{//jika benar mengisi form maka akan ke model datadaftar dengan fungsi insert data yang sudah di buat
			// memanggil model datadaftar yang isinya fungsi insert data dan parameter disamakam dengan model
			 $this->datadaftar->daftar_aksi('bukutamu',$data);
			 redirect('awalan/index');
			}


		}

	}

?>