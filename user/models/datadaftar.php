<?php
	// defined('BASEPATH') OR exit('No direct script access allowed');


class Datadaftar extends CI_Model
{
	function __construct(){
	parent::__construct();
	
}

	function daftar_aksi($table,$data){ // fungsi untuk insert data
		
		$id = $this->input->post('id'); // syntax untuk memasukan data ke database
		$nama = $this->input->post('nama');
		$email = $this->input->post('email');
		$alamat = $this->input->post('alamat');

		$data=array(
			'id'=>$id,
			'nama'=>$nama,
			'email'=>$email,
			'alamat'=>$alamat
		);



	 $this->db->insert($table,$data); // syntax database untuk memasukan datanya
	
}

	function tampildata($per_page,$offset){  //  menampilkan data secara pagination
	
		
		$this->db->offset($offset); //menampilakan data ke n dari tabel
		$this->db->order_by('id','DESC'); 
		$query=$this->db->get('bukutamu',$per_page);
		return $query ->result();
	}

	function baris(){
	
		return $this->db->get('bukutamu')->num_rows(); //mengambil jumlah baris pada database
	}

	function hapus($where,$table){

	
		$this->db->where($where);
		$this->db->delete($table,$where);

	}

	function edit_data($where,$table){  
	 return $this->db->get_where($table,$where);
	}

	 function update_data($where,$data,$table){  // update data dengan kembali mengirimkan data kembali

	 	 $id = $this->input->post('id');
		 $nama = $this->input->post('nama');
		 $email = $this->input->post('email');
		 $alamat = $this->input->post('alamat');
	

		 $data = array(
		  'nama' => $nama,
		  'email' => $email,
		  'alamat' => $alamat,
		
		 );

		 $where = array(
		  'id' => $id
		 );
	  
	  $this->db->update($table,$data,$where);//syntax db untuk update dengan kondisi where
	 } 

	 function ceklogin($table,$where){
				return $this->db->get_where($table,$where);
		}
}
?>